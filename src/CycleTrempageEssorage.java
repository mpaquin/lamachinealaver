public class CycleTrempageEssorage extends Cycle {

	private final static int cadence = 10;
	private final static int temperatureMin = 20;
	private final static int temperatureMax = 30;
	private final static double tempsEssorage = 15;
	private final static double tempsLavage = 10;
	private final static int vitesseRotation = 30;

	public CycleTrempageEssorage() {
		super(cadence, temperatureMax, temperatureMin, tempsEssorage,
				tempsLavage, vitesseRotation);
	}

	@Override
	public void communicationChip() {

	}

}
