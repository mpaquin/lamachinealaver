public class CycleDesinfection extends Cycle {

	private final static int cadence = 0;
	private final static int temperatureMin = 90;
	private final static int temperatureMax = 90;
	private final static double tempsEssorage = 15;
	private final static double tempsLavage = 10;
	private final static int vitesseRotation = 30;

	public CycleDesinfection() {
		super(cadence, temperatureMax, temperatureMin, tempsEssorage,
				tempsLavage, vitesseRotation);
	}

	@Override
	public void communicationChip() {

	}

}