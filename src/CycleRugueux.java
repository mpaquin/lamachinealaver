public class CycleRugueux extends Cycle {

	private final static int cadence = 8;
	private final static int temperatureMin = 30;
	private final static int temperatureMax = 40;
	private final static double tempsEssorage = 10;
	private final static double tempsLavage = 45;
	private final static int vitesseRotation = 20;

	public CycleRugueux() {
		super(cadence, temperatureMax, temperatureMin, tempsEssorage,
				tempsLavage, vitesseRotation);
	}

	@Override
	public void communicationChip() {

	}

}
