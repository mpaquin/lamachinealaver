public class CycleSynthetique extends Cycle {

	private final static int cadence = 15;
	private final static int temperatureMin = 50;
	private final static int temperatureMax = 60;
	private final static double tempsEssorage = 5;
	private final static double tempsLavage = 30;
	private final static int vitesseRotation = 10;

	public CycleSynthetique() {
		super(cadence, temperatureMax, temperatureMin, tempsEssorage,
				tempsLavage, vitesseRotation);
	}

	@Override
	public void communicationChip() {

	}

}
