public interface Constants {

	public static final int CHIP_0x0100 = 0;
	public static final int CHIP_0x0200 = 1;
	public static final int CHIP_0x0700 = 6;

	// 0X0100
	public static final int PIN_EAU_FROIDE = 2;
	public static final int PIN_EAU_CHAUDE = 3;

	// 0X0200
	public static final int PIN_NIVEAU_EAU_0 = 0;
	public static final int PIN_NIVEAU_EAU_1 = 1;
	public static final int PIN_NIVEAU_EAU_2 = 2;
	public static final int PIN_NIVEAU_EAU_3 = 3;
	public static final int PIN_ON_OFF = 4;
	public static final int PIN_COTON = 5;
	public static final int PIN_SYNTHETIQUE = 6;
	public static final int PIN_RUGUEUX = 7;

	// 0X0700
	public static final int PIN_DESINFECTION = 0;
	public static final int PIN_CAPTEUR_NIVEAU_EAU_0 = 4;
	public static final int PIN_CAPTEUR_NIVEAU_EAU_1 = 3;
	public static final int PIN_CAPTEUR_NIVEAU_EAU_2 = 2;
	public static final int PIN_CAPTEUR_NIVEAU_EAU_3 = 1;
	public static final int PIN_TREMPAGE_ESSORAGE = 5;

}
