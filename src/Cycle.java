public abstract class Cycle {

	private int temperature;
	private int temperatureMin;
	private int temperatureMax;
	private double tempsLavage;
	private double tempsEssorage;
	private String niveauEau;
	private int cadence;
	private int vitesseRotation;

	public Cycle(int cadence, int temperatureMax, int temperatureMin,
			double tempsEssorage, double tempsLavage, int vitesseRotation) {
		super();
		this.cadence = cadence;
		this.temperatureMax = temperatureMax;
		this.temperatureMin = temperatureMin;
		this.tempsEssorage = tempsEssorage;
		this.tempsLavage = tempsLavage;
		this.vitesseRotation = vitesseRotation;
	}

	abstract public void communicationChip();

	public int getTemperature() {
		return temperature;
	}

	public void setTemperature(int temperature) {
		this.temperature = temperature;
	}

	public int getTemperatureMin() {
		return temperatureMin;
	}

	public void setTemperatureMin(int temperatureMin) {
		this.temperatureMin = temperatureMin;
	}

	public int getTemperatureMax() {
		return temperatureMax;
	}

	public void setTemperatureMax(int temperatureMax) {
		this.temperatureMax = temperatureMax;
	}

	public double getTempsLavage() {
		return tempsLavage;
	}

	public void setTempsLavage(double tempsLavage) {
		this.tempsLavage = tempsLavage;
	}

	public double getTempsEssorage() {
		return tempsEssorage;
	}

	public void setTempsEssorage(double tempsEssorage) {
		this.tempsEssorage = tempsEssorage;
	}

	public String getNiveauEau() {
		return niveauEau;
	}

	public void setNiveauEau(String niveauEau) {
		this.niveauEau = niveauEau;
	}

	public int getCadence() {
		return cadence;
	}

	public void setCadence(int cadence) {
		this.cadence = cadence;
	}

	public int getVitesseRotation() {
		return vitesseRotation;
	}

	public void setVitesseRotation(int vitesseRotation) {
		this.vitesseRotation = vitesseRotation;
	}

}
