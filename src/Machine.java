import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import org.apache.commons.io.IOUtils;

public class Machine extends Thread {

	static Vue vue;

	static File memoire;
	static FileWriter writer;
	static FileReader reader;

	static ArrayList<String> oldContent;

	private Cycle cycleEnCours;
	private boolean enFonction;
	private boolean couvercleOuvert;
	private String niveauDeau = "0000";
	private String status;

	@Override
	public void run() {
		while (true) {
			memoire = new File("memoire.txt");
			synchronized (this) {
				try {
					// writer = new FileWriter(memoire, true);
					reader = new FileReader(memoire);
					ArrayList<String> content = (ArrayList<String>) IOUtils
							.readLines(reader);

					if (!content.equals(oldContent) && oldContent != null) {

						System.out
								.println("-----------------------------------");

						char[] oc = oldContent.get(0).toCharArray();
						char[] c = content.get(0).toCharArray();
						for (int i = 0; i < oc.length; i++) {
							if (oc[i] != c[i]) {
								change0x0100(i, c[i], content);
								System.out
										.println("0x0100 " + i + " : " + c[i]);
							}
						}

						oc = oldContent.get(1).toCharArray();
						c = content.get(1).toCharArray();
						for (int i = 0; i < oc.length; i++) {
							if (oc[i] != c[i]) {
								change0x0200(i, c[i], content);
								System.out
										.println("0x0200 " + i + " : " + c[i]);
							}
						}

						oc = oldContent.get(6).toCharArray();
						c = content.get(6).toCharArray();
						for (int i = 0; i < oc.length; i++) {
							if (oc[i] != c[i]) {
								change0x0700(i, c[i], content);
								System.out
										.println("0x0700 " + i + " : " + c[i]);
							}
						}
					}

					oldContent = content;

					reader.close();
					memoire = null;

					wait(1000);
				} catch (IOException | InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
	}

	private void change0x0100(int pin, char value, ArrayList<String> content) {
		switch (pin) {
		case 2:
			// froid
			break;
		case 3:
			// chaud
			break;
		}
	}

	private void change0x0200(int pin, char value, ArrayList<String> content) {
		switch (pin) {
		case 0:
			// niveau d'eau
			this.niveauDeau = String.valueOf(value)
					+ String.valueOf(content.get(Constants.CHIP_0x0200).charAt(
							Constants.PIN_NIVEAU_EAU_1))
					+ String.valueOf(content.get(Constants.CHIP_0x0200).charAt(
							Constants.PIN_NIVEAU_EAU_2))
					+ String.valueOf(content.get(Constants.CHIP_0x0200).charAt(
							Constants.PIN_NIVEAU_EAU_3));

			break;
		case 1:
			// niveau d'eau
			this.niveauDeau = String.valueOf(content.get(Constants.CHIP_0x0200)
					.charAt(Constants.PIN_NIVEAU_EAU_0))
					+ String.valueOf(value)
					+ String.valueOf(content.get(Constants.CHIP_0x0200).charAt(
							Constants.PIN_NIVEAU_EAU_2))
					+ String.valueOf(content.get(Constants.CHIP_0x0200).charAt(
							Constants.PIN_NIVEAU_EAU_3));

			break;
		case 2:
			// niveau d'eau
			this.niveauDeau = String.valueOf(content.get(Constants.CHIP_0x0200)
					.charAt(Constants.PIN_NIVEAU_EAU_0))
					+ String.valueOf(content.get(Constants.CHIP_0x0200).charAt(
							Constants.PIN_NIVEAU_EAU_1))
					+ String.valueOf(value)
					+ String.valueOf(content.get(Constants.CHIP_0x0200).charAt(
							Constants.PIN_NIVEAU_EAU_3));

			break;
		case 3:
			// niveau d'eau

			this.niveauDeau = String.valueOf(content.get(Constants.CHIP_0x0200)
					.charAt(Constants.PIN_NIVEAU_EAU_0))
					+ String.valueOf(content.get(Constants.CHIP_0x0200).charAt(
							Constants.PIN_NIVEAU_EAU_1))
					+ String.valueOf(content.get(Constants.CHIP_0x0200).charAt(
							Constants.PIN_NIVEAU_EAU_2))
					+ String.valueOf(value);

			break;
		case 4:
			// on/off
			if (value == '0')
				this.arreter();
			else
				this.demarrer();

			break;
		case 5:
			// coton

			this.changerCycle(new CycleCoton());

			break;
		case 6:
			// synthetique

			this.changerCycle(new CycleSynthetique());

			break;
		case 7:
			// rugueux

			this.changerCycle(new CycleRugueux());

			break;
		}
	}

	private void change0x0700(int pin, char value, ArrayList<String> content) {
		switch (pin) {
		case 0:
			// desinfection
			
			this.changerCycle(new CycleDesinfection());
			
			break;
		case 1:
			// capteur niveau d'eau

			vue.newWaterLevelDetected();

			break;
		case 2:
			// capteur niveau d'eau

			vue.newWaterLevelDetected();

			break;
		case 3:
			// capteur niveau d'eau

			vue.newWaterLevelDetected();

			break;
		case 4:
			// capteur niveau d'eau

			vue.newWaterLevelDetected();

			break;
		case 5:
			// trempage/essorage
			
			this.changerCycle(new CycleTrempageEssorage());
			
			break;
		}
	}

	public String getEtat() {
		return status;
	}

	public void changerCycle(Cycle cycle) {
		cycleEnCours = cycle;
	}

	public Cycle getCycleEnCours() {
		return cycleEnCours;
	}

	public void changerNiveauDeau(String niveau) {
		this.niveauDeau = niveau;
	}

	public String getNiveauDeau() {
		return this.niveauDeau;
	}

	public void demarrer() {
		enFonction = true;
		status = "En marche";
	}

	public void arreter() {
		enFonction = false;
		status = "Arr�ter";
	}

	public boolean isEnFonction() {
		return enFonction;
	}

	public boolean isCouvercleOuvert() {
		return couvercleOuvert;
	}

	public void ouvrirCouvercle() {
		couvercleOuvert = true;
		status = "Couvercle ouvert";
	}

	public void fermerCouvercle() {
		couvercleOuvert = false;
		status = "Couvercle ferm�";
	}

	public void setVue(Vue ex) {
		vue = ex;
	}
}
