public class CycleCoton extends Cycle {

	private final static int cadence = 10;
	private final static int temperatureMin = 40;
	private final static int temperatureMax = 50;
	private final static double tempsEssorage = 10;
	private final static double tempsLavage = 45;
	private final static int vitesseRotation = 20;

	public CycleCoton() {
		super(cadence, temperatureMax, temperatureMin, tempsEssorage,
				tempsLavage, vitesseRotation);
	}

	@Override
	public void communicationChip() {

	}

}
