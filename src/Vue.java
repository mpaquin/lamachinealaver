import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.Timer;

import org.apache.commons.io.IOUtils;

@SuppressWarnings("serial")
public class Vue extends JFrame {

	static int i = 0;
	static Timer timer, timer2;
	static File memoire;
	static FileWriter writer;
	static FileReader reader;
	static Machine machine;
	static ArrayList<String> content;
	private AbstractButton btnNiveau1;
	private AbstractButton btnNiveau3;
	private AbstractButton btnNiveau2;
	private AbstractButton btnNiveau4;
	private AbstractButton btnNiveau6;
	private AbstractButton btnNiveau5;
	private AbstractButton btnNiveau7;
	private AbstractButton btnNiveau8;
	private AbstractButton btnNiveau9;
	private AbstractButton btnNiveau10;
    private AbstractButton btnDemarrer;
    private JTextField tfNiveauEau;

	public Vue() {

		setTitle("LaMachineALaver");
		setSize(800, 700);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(EXIT_ON_CLOSE);

		getContentPane().setLayout(null);

		JPanel panelCycles = new JPanel();
		panelCycles.setLayout(null);
		panelCycles.setBorder(BorderFactory.createTitledBorder("Cycle"));
		panelCycles.setSize(250, 560);
		getContentPane().add(panelCycles);

		JPanel panelPower = new JPanel();
		panelPower.setLayout(null);
		panelPower.setBorder(BorderFactory.createTitledBorder("Power"));
		panelPower.setBounds(260, 0, 250, 260);
		getContentPane().add(panelPower);

		JPanel panelCouvercle = new JPanel();
		panelCouvercle.setLayout(null);
		panelCouvercle.setBorder(BorderFactory.createTitledBorder("Couvercle"));
		panelCouvercle.setBounds(260, 300, 250, 260);
		getContentPane().add(panelCouvercle);

		JPanel panelStatus = new JPanel();
		panelStatus.setLayout(null);
		panelStatus.setBorder(BorderFactory.createTitledBorder("Status"));
		panelStatus.setBounds(520, 0, 260, 560);
		getContentPane().add(panelStatus);

		JPanel panelNiveauEau = new JPanel();
		panelNiveauEau.setLayout(null);
		panelNiveauEau.setBorder(BorderFactory
				.createTitledBorder("Niveau d'eau (en gallon)"));
		panelNiveauEau.setBounds(0, 570, 780, 90);
		getContentPane().add(panelNiveauEau);

		final JButton btnCoton = new JButton("Coton");
		btnCoton.setBounds(50, 50, 150, 60);
		panelCycles.add(btnCoton);

		final JButton btnRugueux = new JButton("Rugueux");
		btnRugueux.setBounds(50, 130, 150, 60);
		panelCycles.add(btnRugueux);

		final JButton btnTrempageEssorage = new JButton("Trempage essorage");
		btnTrempageEssorage.setBounds(50, 210, 150, 60);
		panelCycles.add(btnTrempageEssorage);

		final JButton btnSynthetique = new JButton("Synthetique");
		btnSynthetique.setBounds(50, 290, 150, 60);
		panelCycles.add(btnSynthetique);

		final JButton btnDesinfection = new JButton("Desinfection");
		btnDesinfection.setBounds(50, 370, 150, 60);
		panelCycles.add(btnDesinfection);

		btnDemarrer = new JButton("Démarrer");
		btnDemarrer.setEnabled(false);
		btnDemarrer.setBounds(50, 50, 150, 60);
		panelPower.add(btnDemarrer);

		JButton btnArreter = new JButton("Arrêter");
		btnArreter.setBounds(50, 130, 150, 60);
		panelPower.add(btnArreter);

		JButton btnOuvrirCouvercle = new JButton("Ouvrir Couvercle");
		btnOuvrirCouvercle.setBounds(50, 50, 150, 60);
		panelCouvercle.add(btnOuvrirCouvercle);

		JButton btnFermerCouvercle = new JButton("Fermer Couvercle");
		btnFermerCouvercle.setBounds(50, 130, 150, 60);
		panelCouvercle.add(btnFermerCouvercle);

		JLabel labelCycle = new JLabel("Cycle actuelle:");
		labelCycle.setBounds(30, 50, 100, 24);
		panelStatus.add(labelCycle);
		final JTextField tfCycle = new JTextField();
		tfCycle.setBounds(130, 50, 100, 24);
		tfCycle.setEditable(false);
		panelStatus.add(tfCycle);

		JLabel labelmachine = new JLabel("État machine:");
		labelmachine.setBounds(30, 100, 100, 24);
		panelStatus.add(labelmachine);
		final JTextField tfMachine = new JTextField();
		tfMachine.setText("Fermé");
		tfMachine.setBounds(130, 100, 100, 24);
		tfMachine.setEditable(false);
		panelStatus.add(tfMachine);

		JLabel labelCouvercle = new JLabel("État couvercle:");
		labelCouvercle.setBounds(30, 150, 100, 24);
		panelStatus.add(labelCouvercle);
		final JTextField tfCouvercle = new JTextField();
		tfCouvercle.setText("Fermé");
		tfCouvercle.setBounds(130, 150, 100, 24);
		tfCouvercle.setEditable(false);
		panelStatus.add(tfCouvercle);

		JLabel labelNiveauEau = new JLabel("Niveau d'eau:");
		labelNiveauEau.setBounds(30, 200, 100, 24);
		panelStatus.add(labelNiveauEau);
		tfNiveauEau = new JTextField();
		tfNiveauEau.setText("Vide");
		tfNiveauEau.setBounds(130, 200, 100, 24);
		tfNiveauEau.setEditable(false);
		panelStatus.add(tfNiveauEau);

		JLabel labelTemps = new JLabel("Temps lavage:");
		labelTemps.setBounds(30, 250, 100, 24);
		panelStatus.add(labelTemps);
		final JTextField tfTemps = new JTextField();
		tfTemps.setText("00:00");
		tfTemps.setBounds(130, 250, 100, 24);
		tfTemps.setEditable(false);
		panelStatus.add(tfTemps);

		JLabel labelTempsEssorage = new JLabel("Temps essorage:");
		labelTempsEssorage.setBounds(30, 300, 100, 24);
		panelStatus.add(labelTempsEssorage);
		final JTextField tfTempsEssorage = new JTextField();
		tfTempsEssorage.setText("00:00");
		tfTempsEssorage.setBounds(130, 300, 100, 24);
		tfTempsEssorage.setEditable(false);
		panelStatus.add(tfTempsEssorage);

		btnNiveau1 = new JButton("01");
		btnNiveau1.setBounds(15, 33, 50, 30);
		panelNiveauEau.add(btnNiveau1);

		btnNiveau2 = new JButton("02");
		btnNiveau2.setBounds(75, 33, 50, 30);
		panelNiveauEau.add(btnNiveau2);

		btnNiveau3 = new JButton("03");
		btnNiveau3.setBounds(135, 33, 50, 30);
		panelNiveauEau.add(btnNiveau3);

		btnNiveau4 = new JButton("04");
		btnNiveau4.setBounds(195, 33, 50, 30);
		panelNiveauEau.add(btnNiveau4);

		btnNiveau5 = new JButton("05");
		btnNiveau5.setBounds(255, 33, 50, 30);
		panelNiveauEau.add(btnNiveau5);

		btnNiveau6 = new JButton("06");
		btnNiveau6.setBounds(315, 33, 50, 30);
		panelNiveauEau.add(btnNiveau6);

		btnNiveau7 = new JButton("07");
		btnNiveau7.setBounds(375, 33, 50, 30);
		panelNiveauEau.add(btnNiveau7);

		btnNiveau8 = new JButton("08");
		btnNiveau8.setBounds(435, 33, 50, 30);
		panelNiveauEau.add(btnNiveau8);

		btnNiveau9 = new JButton("09");
		btnNiveau9.setBounds(495, 33, 50, 30);
		panelNiveauEau.add(btnNiveau9);

		btnNiveau10 = new JButton("10");
		btnNiveau10.setBounds(555, 33, 50, 30);
		panelNiveauEau.add(btnNiveau10);

		int delay = 10;
		ActionListener taskPerformer = new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				int temps = (int) (machine.getCycleEnCours().getTempsLavage() * 60);

				int tempsRestant = temps - i;

				if (tempsRestant == 0) {
					i = 0;
					timer.stop();
					timer2.start();
				}

				tfTemps.setText((int) Math.floor(tempsRestant / 60) + ":"
						+ tempsRestant % 60);

				i++;
			}
		};
		timer = new Timer(delay, taskPerformer);

		ActionListener taskPerformer2 = new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				int temps = (int) (machine.getCycleEnCours().getTempsEssorage() * 60);

				int tempsRestant = temps - i;

				if (tempsRestant == 0) {
					timer2.stop();
					i = 0;
					tfMachine.setText("Arrêtée");
					tfNiveauEau.setText("Vide");

					char[] chars2 = content.get(Constants.CHIP_0x0200)
							.toCharArray();
					// Puts the system in ON mode
					chars2[Constants.PIN_ON_OFF] = '0';
                    chars2[Constants.PIN_NIVEAU_EAU_0] = '0';
                    chars2[Constants.PIN_NIVEAU_EAU_1] = '0';
                    chars2[Constants.PIN_NIVEAU_EAU_2] = '0';
                    chars2[Constants.PIN_NIVEAU_EAU_3] = '0';
					content.set(Constants.CHIP_0x0200, String.valueOf(chars2));

					pushMemoire();

					btnCoton.setEnabled(true);
					btnDesinfection.setEnabled(true);
					btnRugueux.setEnabled(true);
					btnSynthetique.setEnabled(true);
					btnTrempageEssorage.setEnabled(true);

                    btnDemarrer.setEnabled(false);

                    waterLevelMasterEnable(true);
				}

				tfTempsEssorage.setText((int) Math.floor(tempsRestant / 60)
						+ ":" + tempsRestant % 60);

				i++;
			}
		};
		timer2 = new Timer(delay, taskPerformer2);

		btnCoton.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				char[] chars1 = content.get(Constants.CHIP_0x0100)
						.toCharArray();
				// Asks for cold and hot water
				chars1[Constants.PIN_EAU_CHAUDE] = '1';
				chars1[Constants.PIN_EAU_FROIDE] = '1';
				content.set(Constants.CHIP_0x0100, String.valueOf(chars1));

				char[] chars2 = content.get(Constants.CHIP_0x0200)
						.toCharArray();
				// Resets the level of the water
                chars2[Constants.PIN_NIVEAU_EAU_0] = '0';
                chars2[Constants.PIN_NIVEAU_EAU_1] = '0';
                chars2[Constants.PIN_NIVEAU_EAU_2] = '0';
                chars2[Constants.PIN_NIVEAU_EAU_3] = '0';

				// Puts the current mode in Coton
				chars2[Constants.PIN_COTON] = '1';
				chars2[Constants.PIN_SYNTHETIQUE] = '0';
				chars2[Constants.PIN_RUGUEUX] = '0';
				content.set(Constants.CHIP_0x0200, String.valueOf(chars2));

				char[] chars3 = content.get(Constants.CHIP_0x0700)
						.toCharArray();
				chars3[Constants.PIN_DESINFECTION] = '0';
				chars3[Constants.PIN_TREMPAGE_ESSORAGE] = '0';
				chars3[Constants.PIN_CAPTEUR_NIVEAU_EAU_0] = '0';
				chars3[Constants.PIN_CAPTEUR_NIVEAU_EAU_1] = '0';
				chars3[Constants.PIN_CAPTEUR_NIVEAU_EAU_2] = '0';
				chars3[Constants.PIN_CAPTEUR_NIVEAU_EAU_3] = '0';
				content.set(Constants.CHIP_0x0700, String.valueOf(chars3));

				pushMemoire();

				// Execute when button is pressed
				tfCycle.setText("Coton");
                if (isMachineReadyToStart())
                    btnDemarrer.setEnabled(true);
				i = 0;
			}
		});

		btnRugueux.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				char[] chars1 = content.get(Constants.CHIP_0x0100)
						.toCharArray();
				// Asks for cold and hot water
				chars1[Constants.PIN_EAU_CHAUDE] = '1';
				chars1[Constants.PIN_EAU_FROIDE] = '1';
				content.set(Constants.CHIP_0x0100, String.valueOf(chars1));

				char[] chars2 = content.get(Constants.CHIP_0x0200)
						.toCharArray();
				// Resets the level of the water
				chars2[Constants.PIN_NIVEAU_EAU_0] = '0';
				chars2[Constants.PIN_NIVEAU_EAU_1] = '0';
				chars2[Constants.PIN_NIVEAU_EAU_2] = '0';
				chars2[Constants.PIN_NIVEAU_EAU_3] = '0';

				// Puts the current mode in Rugueux
				chars2[Constants.PIN_COTON] = '0';
				chars2[Constants.PIN_SYNTHETIQUE] = '0';
				chars2[Constants.PIN_RUGUEUX] = '1';
				content.set(Constants.CHIP_0x0200, String.valueOf(chars2));

				char[] chars3 = content.get(Constants.CHIP_0x0700)
						.toCharArray();
				chars3[Constants.PIN_DESINFECTION] = '0';
				chars3[Constants.PIN_TREMPAGE_ESSORAGE] = '0';
				chars3[Constants.PIN_CAPTEUR_NIVEAU_EAU_0] = '0';
				chars3[Constants.PIN_CAPTEUR_NIVEAU_EAU_1] = '0';
				chars3[Constants.PIN_CAPTEUR_NIVEAU_EAU_2] = '0';
				chars3[Constants.PIN_CAPTEUR_NIVEAU_EAU_3] = '0';
				content.set(Constants.CHIP_0x0700, String.valueOf(chars3));

				pushMemoire();

				// Execute when button is pressed
				tfCycle.setText("Rugueux");
                if (isMachineReadyToStart())
                    btnDemarrer.setEnabled(true);
				i = 0;
			}
		});

		btnTrempageEssorage.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				char[] chars1 = content.get(Constants.CHIP_0x0100)
						.toCharArray();
				// Asks for cold and hot water
				chars1[Constants.PIN_EAU_CHAUDE] = '1';
				chars1[Constants.PIN_EAU_FROIDE] = '1';
				content.set(Constants.CHIP_0x0100, String.valueOf(chars1));

				char[] chars2 = content.get(Constants.CHIP_0x0200)
						.toCharArray();
				// Resets the level of the water
				chars2[Constants.PIN_NIVEAU_EAU_0] = '0';
				chars2[Constants.PIN_NIVEAU_EAU_1] = '0';
				chars2[Constants.PIN_NIVEAU_EAU_2] = '0';
				chars2[Constants.PIN_NIVEAU_EAU_3] = '0';

				// Puts the current mode in Trempage/Essorage
				chars2[Constants.PIN_COTON] = '0';
				chars2[Constants.PIN_SYNTHETIQUE] = '0';
				chars2[Constants.PIN_RUGUEUX] = '0';
				content.set(Constants.CHIP_0x0200, String.valueOf(chars2));

				char[] chars3 = content.get(Constants.CHIP_0x0700)
						.toCharArray();
				chars3[Constants.PIN_DESINFECTION] = '0';
				chars3[Constants.PIN_TREMPAGE_ESSORAGE] = '1';
				chars3[Constants.PIN_CAPTEUR_NIVEAU_EAU_0] = '0';
				chars3[Constants.PIN_CAPTEUR_NIVEAU_EAU_1] = '0';
				chars3[Constants.PIN_CAPTEUR_NIVEAU_EAU_2] = '0';
				chars3[Constants.PIN_CAPTEUR_NIVEAU_EAU_3] = '0';
				content.set(Constants.CHIP_0x0700, String.valueOf(chars3));

				pushMemoire();

				// Execute when button is pressed
				tfCycle.setText("Trempage Essorage");
                if (isMachineReadyToStart())
                    btnDemarrer.setEnabled(true);
				i = 0;
			}
		});

		btnSynthetique.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				char[] chars1 = content.get(Constants.CHIP_0x0100)
						.toCharArray();
				// Asks for cold and hot water
				chars1[Constants.PIN_EAU_CHAUDE] = '1';
				chars1[Constants.PIN_EAU_FROIDE] = '1';
				content.set(Constants.CHIP_0x0100, String.valueOf(chars1));

				char[] chars2 = content.get(Constants.CHIP_0x0200)
						.toCharArray();
				// Resets the level of the water
				chars2[Constants.PIN_NIVEAU_EAU_0] = '0';
				chars2[Constants.PIN_NIVEAU_EAU_1] = '0';
				chars2[Constants.PIN_NIVEAU_EAU_2] = '0';
				chars2[Constants.PIN_NIVEAU_EAU_3] = '0';

				// Puts the current mode in Synthetique
				chars2[Constants.PIN_COTON] = '0';
				chars2[Constants.PIN_SYNTHETIQUE] = '1';
				chars2[Constants.PIN_RUGUEUX] = '0';
				content.set(Constants.CHIP_0x0200, String.valueOf(chars2));

				char[] chars3 = content.get(Constants.CHIP_0x0700)
						.toCharArray();
				chars3[Constants.PIN_DESINFECTION] = '0';
				chars3[Constants.PIN_TREMPAGE_ESSORAGE] = '0';
				chars3[Constants.PIN_CAPTEUR_NIVEAU_EAU_0] = '0';
				chars3[Constants.PIN_CAPTEUR_NIVEAU_EAU_1] = '0';
				chars3[Constants.PIN_CAPTEUR_NIVEAU_EAU_2] = '0';
				chars3[Constants.PIN_CAPTEUR_NIVEAU_EAU_3] = '0';
				content.set(Constants.CHIP_0x0700, String.valueOf(chars3));

				pushMemoire();

				// Execute when button is pressed
				tfCycle.setText("Synthetique");
                if (isMachineReadyToStart())
                    btnDemarrer.setEnabled(true);
				i = 0;
			}
		});

		btnDesinfection.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				char[] chars1 = content.get(Constants.CHIP_0x0100)
						.toCharArray();
				// Asks for cold and hot water
				chars1[Constants.PIN_EAU_CHAUDE] = '1';
				chars1[Constants.PIN_EAU_FROIDE] = '0';
				content.set(Constants.CHIP_0x0100, String.valueOf(chars1));

				char[] chars2 = content.get(Constants.CHIP_0x0200)
						.toCharArray();
				// Resets the level of the water
				chars2[Constants.PIN_NIVEAU_EAU_0] = '0';
				chars2[Constants.PIN_NIVEAU_EAU_1] = '0';
				chars2[Constants.PIN_NIVEAU_EAU_2] = '0';
				chars2[Constants.PIN_NIVEAU_EAU_3] = '0';

				// Puts the current mode in Desinfection
				chars2[Constants.PIN_COTON] = '0';
				chars2[Constants.PIN_SYNTHETIQUE] = '0';
				chars2[Constants.PIN_RUGUEUX] = '0';
				content.set(Constants.CHIP_0x0200, String.valueOf(chars2));

				char[] chars3 = content.get(Constants.CHIP_0x0700)
						.toCharArray();
				chars3[Constants.PIN_DESINFECTION] = '1';
				chars3[Constants.PIN_TREMPAGE_ESSORAGE] = '0';
				chars3[Constants.PIN_CAPTEUR_NIVEAU_EAU_0] = '0';
				chars3[Constants.PIN_CAPTEUR_NIVEAU_EAU_1] = '0';
				chars3[Constants.PIN_CAPTEUR_NIVEAU_EAU_2] = '0';
				chars3[Constants.PIN_CAPTEUR_NIVEAU_EAU_3] = '0';
				content.set(Constants.CHIP_0x0700, String.valueOf(chars3));

				pushMemoire();

				// Execute when button is pressed
				tfCycle.setText("Desinfection");
                if (isMachineReadyToStart())
                    btnDemarrer.setEnabled(true);
				i = 0;
			}
		});

		btnDemarrer.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				// Execute when button is pressed
				if (!machine.isCouvercleOuvert()) {
					char[] chars2 = content.get(Constants.CHIP_0x0200)
							.toCharArray();
					// Puts the system in ON mode
					chars2[Constants.PIN_ON_OFF] = '1';
					content.set(Constants.CHIP_0x0200, String.valueOf(chars2));

					pushMemoire();

					tfMachine.setText("En fonction");
					timer.start();
					btnCoton.setEnabled(false);
					btnDesinfection.setEnabled(false);
					btnRugueux.setEnabled(false);
					btnSynthetique.setEnabled(false);
					btnTrempageEssorage.setEnabled(false);

                    waterLevelMasterEnable(false);
				} else {
					System.out.println("Le couvercle doit être fermé.");
				}
			}
		});

		btnArreter.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				char[] chars2 = content.get(Constants.CHIP_0x0200)
						.toCharArray();
				// Puts the system in ON mode
				chars2[Constants.PIN_ON_OFF] = '0';
                chars2[Constants.PIN_NIVEAU_EAU_0] = '0';
                chars2[Constants.PIN_NIVEAU_EAU_1] = '0';
                chars2[Constants.PIN_NIVEAU_EAU_2] = '0';
                chars2[Constants.PIN_NIVEAU_EAU_3] = '0';
				content.set(Constants.CHIP_0x0200, String.valueOf(chars2));

				pushMemoire();

				// Execute when button is pressed
				tfMachine.setText("Arrêtée");
				timer.stop();
				timer2.stop();

                tfTemps.setText("00:00");
                tfTempsEssorage.setText("00:00");

				i = 0;

				btnCoton.setEnabled(true);
				btnDesinfection.setEnabled(true);
				btnRugueux.setEnabled(true);
				btnSynthetique.setEnabled(true);
				btnTrempageEssorage.setEnabled(true);

                waterLevelMasterEnable(true);

                tfNiveauEau.setText("Vide");

                btnDemarrer.setEnabled(false);
			}
		});

		btnOuvrirCouvercle.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				char[] chars2 = content.get(Constants.CHIP_0x0200)
						.toCharArray();
				// Puts the system in ON mode
				chars2[Constants.PIN_ON_OFF] = '0';
				content.set(Constants.CHIP_0x0200, String.valueOf(chars2));

				pushMemoire();

				// Execute when button is pressed
				machine.ouvrirCouvercle();
				tfCouvercle.setText("Ouvert");
				btnDemarrer.setEnabled(false);

                timer.stop();
                timer2.stop();


			}
		});

		btnFermerCouvercle.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				// Execute when button is pressed
				machine.fermerCouvercle();
				tfCouvercle.setText("Fermé");
                if (isMachineReadyToStart())
                    btnDemarrer.setEnabled(true);
			}
		});

		btnNiveau1.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				char[] chars2 = content.get(Constants.CHIP_0x0200)
						.toCharArray();
				// Resets the level of the water
				chars2[Constants.PIN_NIVEAU_EAU_0] = '0';
				chars2[Constants.PIN_NIVEAU_EAU_1] = '0';
				chars2[Constants.PIN_NIVEAU_EAU_2] = '0';
				chars2[Constants.PIN_NIVEAU_EAU_3] = '1';
				content.set(Constants.CHIP_0x0200, String.valueOf(chars2));

				pushMemoire();

				tfNiveauEau.setText("1 gallon");
				
                if (machine.getCycleEnCours() != null)
                    btnDemarrer.setEnabled(true);
			}
		});

		btnNiveau2.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				char[] chars2 = content.get(Constants.CHIP_0x0200)
						.toCharArray();
				// Resets the level of the water
				chars2[Constants.PIN_NIVEAU_EAU_0] = '0';
				chars2[Constants.PIN_NIVEAU_EAU_1] = '0';
				chars2[Constants.PIN_NIVEAU_EAU_2] = '1';
				chars2[Constants.PIN_NIVEAU_EAU_3] = '0';
				content.set(Constants.CHIP_0x0200, String.valueOf(chars2));

				pushMemoire();

				tfNiveauEau.setText("2 gallons");

                if (machine.getCycleEnCours() != null)
                    btnDemarrer.setEnabled(true);
			}
		});

		btnNiveau3.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				char[] chars2 = content.get(Constants.CHIP_0x0200)
						.toCharArray();
				// Resets the level of the water
				chars2[Constants.PIN_NIVEAU_EAU_0] = '0';
				chars2[Constants.PIN_NIVEAU_EAU_1] = '0';
				chars2[Constants.PIN_NIVEAU_EAU_2] = '1';
				chars2[Constants.PIN_NIVEAU_EAU_3] = '1';
				content.set(Constants.CHIP_0x0200, String.valueOf(chars2));

				pushMemoire();

				tfNiveauEau.setText("3 gallons");

                if (isMachineReadyToStart())
                    btnDemarrer.setEnabled(true);
			}
		});

		btnNiveau4.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				char[] chars2 = content.get(Constants.CHIP_0x0200)
						.toCharArray();
				// Resets the level of the water
				chars2[Constants.PIN_NIVEAU_EAU_0] = '0';
				chars2[Constants.PIN_NIVEAU_EAU_1] = '1';
				chars2[Constants.PIN_NIVEAU_EAU_2] = '0';
				chars2[Constants.PIN_NIVEAU_EAU_3] = '0';
				content.set(Constants.CHIP_0x0200, String.valueOf(chars2));

				pushMemoire();

				tfNiveauEau.setText("4 gallons");

                if (isMachineReadyToStart())
                    btnDemarrer.setEnabled(true);
			}
		});

		btnNiveau5.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				char[] chars2 = content.get(Constants.CHIP_0x0200)
						.toCharArray();
				// Resets the level of the water
				chars2[Constants.PIN_NIVEAU_EAU_0] = '0';
				chars2[Constants.PIN_NIVEAU_EAU_1] = '1';
				chars2[Constants.PIN_NIVEAU_EAU_2] = '0';
				chars2[Constants.PIN_NIVEAU_EAU_3] = '1';
				content.set(Constants.CHIP_0x0200, String.valueOf(chars2));

				pushMemoire();

				tfNiveauEau.setText("5 gallons");

                if (isMachineReadyToStart())
                    btnDemarrer.setEnabled(true);
			}
		});

		btnNiveau6.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				char[] chars2 = content.get(Constants.CHIP_0x0200)
						.toCharArray();
				// Resets the level of the water
				chars2[Constants.PIN_NIVEAU_EAU_0] = '0';
				chars2[Constants.PIN_NIVEAU_EAU_1] = '1';
				chars2[Constants.PIN_NIVEAU_EAU_2] = '1';
				chars2[Constants.PIN_NIVEAU_EAU_3] = '0';
				content.set(Constants.CHIP_0x0200, String.valueOf(chars2));

				pushMemoire();

				tfNiveauEau.setText("6 gallons");

                if (isMachineReadyToStart())
                    btnDemarrer.setEnabled(true);
			}
		});

		btnNiveau7.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				char[] chars2 = content.get(Constants.CHIP_0x0200)
						.toCharArray();
				// Resets the level of the water
				chars2[Constants.PIN_NIVEAU_EAU_0] = '0';
				chars2[Constants.PIN_NIVEAU_EAU_1] = '1';
				chars2[Constants.PIN_NIVEAU_EAU_2] = '1';
				chars2[Constants.PIN_NIVEAU_EAU_3] = '1';
				content.set(Constants.CHIP_0x0200, String.valueOf(chars2));

				pushMemoire();

				tfNiveauEau.setText("7 gallons");

                if (isMachineReadyToStart())
                    btnDemarrer.setEnabled(true);
			}
		});

		btnNiveau8.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				char[] chars2 = content.get(Constants.CHIP_0x0200)
						.toCharArray();
				// Resets the level of the water
				chars2[Constants.PIN_NIVEAU_EAU_0] = '1';
				chars2[Constants.PIN_NIVEAU_EAU_1] = '0';
				chars2[Constants.PIN_NIVEAU_EAU_2] = '0';
				chars2[Constants.PIN_NIVEAU_EAU_3] = '0';
				content.set(Constants.CHIP_0x0200, String.valueOf(chars2));

				pushMemoire();

				tfNiveauEau.setText("8 gallons");

                if (isMachineReadyToStart())
                    btnDemarrer.setEnabled(true);
			}
		});

		btnNiveau9.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				char[] chars2 = content.get(Constants.CHIP_0x0200)
						.toCharArray();
				// Resets the level of the water
				chars2[Constants.PIN_NIVEAU_EAU_0] = '1';
				chars2[Constants.PIN_NIVEAU_EAU_1] = '0';
				chars2[Constants.PIN_NIVEAU_EAU_2] = '0';
				chars2[Constants.PIN_NIVEAU_EAU_3] = '1';
				content.set(Constants.CHIP_0x0200, String.valueOf(chars2));

				pushMemoire();

				tfNiveauEau.setText("9 gallons");

                if (isMachineReadyToStart())
                    btnDemarrer.setEnabled(true);
			}
		});

		btnNiveau10.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				char[] chars2 = content.get(Constants.CHIP_0x0200)
						.toCharArray();
				// Resets the level of the water
				chars2[Constants.PIN_NIVEAU_EAU_0] = '1';
				chars2[Constants.PIN_NIVEAU_EAU_1] = '0';
				chars2[Constants.PIN_NIVEAU_EAU_2] = '1';
				chars2[Constants.PIN_NIVEAU_EAU_3] = '0';
				content.set(Constants.CHIP_0x0200, String.valueOf(chars2));

				pushMemoire();

				tfNiveauEau.setText("10 gallons");

                if (isMachineReadyToStart())
                    btnDemarrer.setEnabled(true);
			}
		});
	}

	private static void pushMemoire() {
		try {
			memoire = new File("memoire.txt");
			writer = new FileWriter(memoire);

			IOUtils.writeLines(content, System.lineSeparator(), writer);
			writer.flush();

			writer.close();
			memoire = null;
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void newWaterLevelDetected() {
		try {
			memoire = new File("memoire.txt");
			reader = new FileReader(memoire);

			content = (ArrayList<String>) IOUtils.readLines(reader);
			if (content.get(Constants.CHIP_0x0700).charAt(
					Constants.PIN_CAPTEUR_NIVEAU_EAU_0) == machine
					.getNiveauDeau().charAt(0)
					&& content.get(Constants.CHIP_0x0700).charAt(
							Constants.PIN_CAPTEUR_NIVEAU_EAU_1) == machine
							.getNiveauDeau().charAt(1)
					&& content.get(Constants.CHIP_0x0700).charAt(
							Constants.PIN_CAPTEUR_NIVEAU_EAU_2) == machine
							.getNiveauDeau().charAt(2)
					&& content.get(Constants.CHIP_0x0700).charAt(
							Constants.PIN_CAPTEUR_NIVEAU_EAU_3) == machine
							.getNiveauDeau().charAt(3)) {
				System.out.println("Niveau OK");

                btnDemarrer.setEnabled(true);

				char[] chars1 = content.get(Constants.CHIP_0x0100)
						.toCharArray();
				// Closes the cold and hot water
				chars1[Constants.PIN_EAU_CHAUDE] = '0';
				chars1[Constants.PIN_EAU_FROIDE] = '0';
				content.set(Constants.CHIP_0x0100, String.valueOf(chars1));
			}

			reader.close();

			pushMemoire();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private void waterLevelMasterEnable(boolean value) {
		btnNiveau1.setEnabled(value);
		btnNiveau2.setEnabled(value);
		btnNiveau3.setEnabled(value);
		btnNiveau4.setEnabled(value);
		btnNiveau5.setEnabled(value);
		btnNiveau6.setEnabled(value);
		btnNiveau7.setEnabled(value);
		btnNiveau8.setEnabled(value);
		btnNiveau9.setEnabled(value);
		btnNiveau10.setEnabled(value);
	}

    private boolean isMachineReadyToStart() {
        if (!machine.isCouvercleOuvert() && (!tfNiveauEau.getText().equals("Vide") || !machine.getNiveauDeau().equals("0000")) && machine.getCycleEnCours() != null)
            return true;

        return false;
    }

	public static void main(String[] args) {
		content = new ArrayList<>();
		content.add(0, "00000000");
		content.add(1, "00000000");
		content.add(2, "");
		content.add(3, "");
		content.add(4, "");
		content.add(5, "");
		content.add(6, "00000000");
		pushMemoire();

		machine = new Machine();
		machine.start();

		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				Vue ex = new Vue();
				machine.setVue(ex);
				ex.setVisible(true);
			}
		});
	}
}